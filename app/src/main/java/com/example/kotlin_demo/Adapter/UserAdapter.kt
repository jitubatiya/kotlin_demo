package com.example.kotlin_demo.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.user_list_item.view.*

class UserAdapter(val items: ArrayList<HashMap<String, String>>, val context: Context) : RecyclerView.Adapter<ViewHolder>()
{

    // Gets the number of animals in the list
    lateinit var obj:CliclEvnt
    override fun getItemCount(): Int {
        return items.size
    }
    interface CliclEvnt{
        fun ClickMe(position: Int,view: View);
    }
    fun setOnItemClick(obj:CliclEvnt)
    {
        this.obj=obj
    }
    // Inflates the item views


    // Binds each animal in the ArrayList to a view

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hashmap=items.get(position)

        holder.main_view.setOnClickListener(){
                obj.ClickMe(position,holder.main_view)
        }
        holder.index.text=(position+1).toString()
        holder.id.text="Id:"+hashmap.get("id")
        holder.name.text="Name:"+hashmap.get("name")
        holder.phone.text="Phone:"+hashmap.get("phone")
        holder.password.text="Password:"+hashmap.get("password")
        holder.email.text="Email:"+hashmap.get("email")
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val id = view.textViewId
    val name = view.textViewName
    val password = view.textViewPassword
    val phone = view.textViewPhone
    val main_view=view.linearLayout
    val index=view.textViewIndex
    val email=view.textViewEmail
}