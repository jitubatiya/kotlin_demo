package com.example.kotlin_demo.SimpleListSetAdapater

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var arr=Array(5){0}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        r1.setOnClickListener() {
            checkValidation()
        }
        txtsignin.setOnClickListener(){
            checkValidation()
        }
        txtsignup.setOnClickListener(){
            val intent = Intent(this, RecyleActivity::class.java)

            startActivity(intent)
        }

    }
    fun checkValidation()
    {
        if(!isValidEmail(edit_email.text.toString()))
           Toast.makeText(this@MainActivity,"Email address invalid.",Toast.LENGTH_SHORT).show()
        else if(edit_password.text.length<=0)
            Toast.makeText(this@MainActivity,"Password is empty..",Toast.LENGTH_SHORT).show()
    }
    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }



}