package com.example.kotlin_demo.SimpleListSetAdapater

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.activity_recyle.*

class RecyleActivity : AppCompatActivity(),
    AnimalAdapter.CliclEvnt {
    val animals= ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recyle)
        addAnimals()
        rv_animal_list.layoutManager=LinearLayoutManager(this)
        var adpter_obj=
            AnimalAdapter(
                animals,
                this
            )

        rv_animal_list.adapter=adpter_obj
        adpter_obj.setOnItemClick(this)
    }
    fun addAnimals() {
        animals.add("dog")
        animals.add("cat")
        animals.add("owl")
        animals.add("cheetah")
        animals.add("raccoon")
        animals.add("bird")
        animals.add("snake")
        animals.add("lizard")
        animals.add("hamster")
        animals.add("bear")
        animals.add("lion")
        animals.add("tiger")
        animals.add("horse")
        animals.add("frog")
        animals.add("fish")
        animals.add("shark")
        animals.add("turtle")
        animals.add("elephant")
        animals.add("cow")
        animals.add("beaver")
        animals.add("bison")
        animals.add("porcupine")
        animals.add("rat")
        animals.add("mouse")
        animals.add("goose")
        animals.add("deer")
        animals.add("fox")
        animals.add("moose")
        animals.add("buffalo")
        animals.add("monkey")
        animals.add("penguin")
        animals.add("parrot")
    }

    override fun ClickMe(position: Int, view: View) {
        Toast.makeText(this,animals.get(position),Toast.LENGTH_SHORT).show()

    }
}