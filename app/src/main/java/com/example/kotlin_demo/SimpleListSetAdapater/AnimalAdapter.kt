package com.example.kotlin_demo.SimpleListSetAdapater

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.animal_list_item.view.*

class AnimalAdapter(val items : ArrayList<String>, val context: Context) : RecyclerView.Adapter<ViewHolder>()
{

    // Gets the number of animals in the list
    lateinit var obj: CliclEvnt
    override fun getItemCount(): Int {
        return items.size
    }
    interface CliclEvnt{
        fun ClickMe(position: Int,view: View);
    }
    fun setOnItemClick(obj: CliclEvnt)
    {
        this.obj=obj
    }
    // Inflates the item views


    // Binds each animal in the ArrayList to a view

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.animal_list_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.tvAnimalType?.text = items.get(position)
        holder.main_view.setOnClickListener(){
                obj.ClickMe(position,holder.main_view)
        }
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val tvAnimalType = view.tv_animal_type
    val main_view=view.main_view
}