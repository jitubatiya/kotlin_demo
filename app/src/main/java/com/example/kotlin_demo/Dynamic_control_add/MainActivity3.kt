package com.example.kotlin_demo.Dynamic_control_add

import android.os.Bundle
import android.util.Log
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlin_demo.R


class MainActivity3 : AppCompatActivity(), CompoundButton.OnCheckedChangeListener {
    var data=ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        var list=ArrayList<CheckBox>()
        val array= arrayOf("hi","Heloo","Good","welcome")
        val linearLayout = findViewById<LinearLayout>(R.id.rootContainer)
        var index:Int=0
        for(i in array)
        {
            val checkBox = CheckBox(this)
            checkBox.setText(i)
            checkBox.setTag(i)
            list.add(checkBox)
            linearLayout?.addView(list.get(index))
            list.get(index).setOnCheckedChangeListener(this)
            index++
        }

    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean)
    {
            var checkBox=buttonView as CheckBox
            if(data.contains(checkBox.tag))
            {
                data.remove(checkBox.tag)
            }
            else
                data.add(checkBox.tag.toString())

            for(i in data)
                Log.d("data",i)
            Toast.makeText(this,""+checkBox.tag.toString(),Toast.LENGTH_LONG).show()
    }


}