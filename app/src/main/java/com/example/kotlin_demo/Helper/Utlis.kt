package com.example.kotlin_demo.Helper

import android.text.TextUtils
import android.util.Patterns

class  Utlis
{
    companion object {
        val phone_reg: String = ""
        fun isValidEmail(email: String): Boolean {
            return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }
        fun isEmptyCheck(text:String):Boolean
        {
            return TextUtils.isEmpty(text)
        }
    }
}