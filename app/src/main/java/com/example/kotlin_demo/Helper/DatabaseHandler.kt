package com.example.kotlin_demo.Helper

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.kotlin_demo.Model.EmpModelClass

class DatabaseHandler(context: Context):SQLiteOpenHelper(context,
    DATABASE_NAME,null,
    DATABASE_VERSION
)
{
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "EmployeeDatabase"
        private val TABLE_CONTACTS = "EmployeeTable"
        private val KEY_ID = "id"
        private val KEY_NAME = "name"
        private val KEY_EMAIL = "email"
        private val KEY_PASSWORD="password"
        private val KEY_PHONE="phone"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_CONTACTS_TABLE = ("CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE,"+ KEY_PASSWORD + " TEXT,"+ KEY_PHONE +" TEXT UNIQUE"+")")
        db?.execSQL(CREATE_CONTACTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS)
        onCreate(db)
    }

    fun addEmployee(emp: EmpModelClass):Long{

        if(!checkEmail(emp.email)) {
            val db = this.writableDatabase
            val contentValues = ContentValues()
            contentValues.put(KEY_ID, emp.id)
            contentValues.put(KEY_NAME, emp.name) // EmpModelClass Name
            contentValues.put(KEY_EMAIL, emp.email) // EmpModelClass email
            contentValues.put(KEY_PHONE, emp.phone) // EmpModelClass phone
            contentValues.put(KEY_PASSWORD, emp.password) // EmpModelClass password
            // Inserting Row
            val success = db.insert(TABLE_CONTACTS, null, contentValues)
            //2nd argument is String containing nullColumnHack
            db.close() // Closing database connection
            return success
        }
        else
            return 404;
    }

    private fun checkPhone(phone: String): Boolean
    {
        val selectQuery = "SELECT  * FROM $TABLE_CONTACTS where phone='"+phone+"'"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return false
        }
        if(cursor.count>0)
            return true
        else
            return false
    }

    private fun checkEmail(email: String): Boolean {
        val selectQuery = "SELECT  * FROM $TABLE_CONTACTS where  email='"+email.trim()+"' "
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return false
        }
        if(cursor.count>0)
            return true
        else
            return false
    }

    fun viewEmployee():List<EmpModelClass>{
        val empList:ArrayList<EmpModelClass> = ArrayList<EmpModelClass>()
        val selectQuery = "SELECT  * FROM $TABLE_CONTACTS"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var userId: Int
        var userName: String
        var userEmail: String
        var userPhone: String
        var userPassword: String
        if (cursor.moveToFirst()) {
            do {
                userId = cursor.getInt(cursor.getColumnIndex("id"))
                userName = cursor.getString(cursor.getColumnIndex("name"))
                userEmail = cursor.getString(cursor.getColumnIndex("email"))
                userPhone = cursor.getString(cursor.getColumnIndex("phone"))
                userPassword = cursor.getString(cursor.getColumnIndex("password"))
                val emp= EmpModelClass(id = userId, name = userName, email = userEmail,phone =userPhone,password = userPassword )
                empList.add(emp)
            } while (cursor.moveToNext())
        }
        return empList
    }
    fun updateEmployee(emp: EmpModelClass):Int{

            val db = this.writableDatabase
            val contentValues = ContentValues()
            contentValues.put(KEY_ID, emp.id)
            contentValues.put(KEY_NAME, emp.name) // EmpModelClass Name
            contentValues.put(KEY_EMAIL, emp.email) // EmpModelClass Email
            contentValues.put(KEY_PHONE, emp.phone)
            contentValues.put(KEY_PASSWORD, emp.password)


            // Updating Row
            try {
                val success = db.update(TABLE_CONTACTS, contentValues, "id=" + emp.id, null)
                //2nd argument is String containing nullColumnHack
                db.close() // Closing database connection
                return success
            }catch (e:Exception)
            {
                return 404
            }

    }
    //method to delete data
    fun deleteEmployee(emp: EmpModelClass):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, emp.id) // EmpModelClass UserId
        // Deleting Row
        val success = db.delete(TABLE_CONTACTS,"id="+emp.id,null)
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
        return success
    }

}