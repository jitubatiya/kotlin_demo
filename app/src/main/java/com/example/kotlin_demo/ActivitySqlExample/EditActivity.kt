package com.example.kotlin_demo.ActivitySqlExample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.kotlin_demo.Helper.DatabaseHandler
import com.example.kotlin_demo.Helper.Utlis
import com.example.kotlin_demo.Model.EmpModelClass
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_insert.*
import kotlinx.android.synthetic.main.activity_insert.ed_email
import kotlinx.android.synthetic.main.activity_insert.ed_name
import kotlinx.android.synthetic.main.activity_insert.ed_password
import kotlinx.android.synthetic.main.activity_insert.ed_phone

class EditActivity : AppCompatActivity() {
    var hashMap=HashMap<String,String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        val intent=getIntent()
        hashMap= intent.getSerializableExtra("userData") as HashMap<String, String>
        ed_phone.setText(hashMap.get("phone"))
        ed_password.setText(hashMap.get("password"))
        ed_name.setText(hashMap.get("name"))
        ed_email.setText(hashMap.get("email"))

        btn_update_data.setOnClickListener()
        {
            if(!checkValidation())
                updateData()
        }
        btn_delete_data.setOnClickListener()
        {
            deleteData()
        }

    }

    private fun deleteData() {
        val deleteId = hashMap.get("id")
        //creating the instance of DatabaseHandler class
        val databaseHandler: DatabaseHandler= DatabaseHandler(this)
        if(deleteId?.trim()!=""){
            //calling the deleteEmployee method of DatabaseHandler class to delete record
            val status = databaseHandler.deleteEmployee(EmpModelClass(Integer.parseInt(deleteId),"","","",""))
            if(status > -1){
                Toast.makeText(applicationContext,"record deleted",Toast.LENGTH_LONG).show()
                val intent=Intent(this,DisplayActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }
        }else{
            Toast.makeText(applicationContext,"id or name or email cannot be blank",Toast.LENGTH_LONG).show()
        }
    }

    private fun updateData() {
        val id = hashMap.get("id")
        val name = ed_name.text.toString()
        val email = ed_email.text.toString()
        val phone = ed_phone.text.toString()
        val password = ed_password.text.toString()
        val databaseHandler: DatabaseHandler = DatabaseHandler(this)
        if(id?.trim()!="" && name.trim()!="" && email.trim()!=""){
            val status = databaseHandler.updateEmployee(EmpModelClass(Integer.parseInt(id),name, email,phone,password))
            if(status.equals(404))
            {
                Toast.makeText(
                    applicationContext,
                    "email or phone number are already taken someone ",
                    Toast.LENGTH_LONG
                ).show()
            }
            else if(status > -1){
                Toast.makeText(applicationContext,"Update Record",Toast.LENGTH_LONG).show()
                val intent=Intent(this,DisplayActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }
        }else{
            Toast.makeText(applicationContext,"id or name or email cannot be blank",Toast.LENGTH_LONG).show()
        }
    }
    private fun checkValidation(): Boolean {

        if(Utlis.isEmptyCheck(ed_name.text.toString().trim()))
        {
            Toast.makeText(this,"Username is requirement.",Toast.LENGTH_SHORT).show()
            return true
        }
        else if(!Utlis.isValidEmail(ed_email.text.toString().trim()))
        {
            Toast.makeText(this,"Email is invalid.",Toast.LENGTH_SHORT).show()
            return true
        }
        else if(Utlis.isEmptyCheck(ed_phone.text.toString().trim()))
        {
            Toast.makeText(this,"Phone Number is requirement.",Toast.LENGTH_SHORT).show()
            return true
        }
        else if(Utlis.isEmptyCheck(ed_password.text.toString().trim()))
        {
            Toast.makeText(this,"Password is requirement.",Toast.LENGTH_SHORT).show()
            return true
        }
        else
        {
            return false
        }

    }
}