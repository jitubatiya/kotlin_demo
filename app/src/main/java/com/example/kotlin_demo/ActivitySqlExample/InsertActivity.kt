package com.example.kotlin_demo.ActivitySqlExample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.kotlin_demo.Helper.DatabaseHandler
import com.example.kotlin_demo.Helper.Utlis
import com.example.kotlin_demo.Model.EmpModelClass
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.activity_insert.*

class InsertActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert)

        btn_add_data.setOnClickListener(){
            if(!checkValidation())
            {
                addData()
            }
        }
    }

    private fun addData()
    {
        val phone = ed_phone.text.toString()
        val name = ed_name.text.toString()
        val email = ed_email.text.toString()
        val password = ed_password.text.toString()
        val databaseHandler = DatabaseHandler(this)

        val status = databaseHandler.addEmployee(EmpModelClass(null,name, email,phone,password))
        Log.d("status",status.toString())
        if(status>=404&&status<=404)
        {
            Toast.makeText(
                applicationContext,
                "email or phone number are already taken someone ",
                Toast.LENGTH_LONG
            ).show()
        }
        else if(status > -1){
            Toast.makeText(applicationContext,"record save",Toast.LENGTH_LONG).show()
            val intent= Intent(this,DisplayActivity::class.java)
            startActivity(intent)
            finishAffinity()

        }
        else {
            Toast.makeText(
                applicationContext,
                "id or name or email cannot be blank",
                Toast.LENGTH_LONG
            ).show()
        }

}

    private fun checkValidation(): Boolean {

        if(Utlis.isEmptyCheck(ed_name.text.toString().trim()))
        {
            Toast.makeText(this,"Username is requirement.",Toast.LENGTH_SHORT).show()
            return true
        }
        else if(!Utlis.isValidEmail(ed_email.text.toString().trim()))
        {
            Toast.makeText(this,"Email is invalid.",Toast.LENGTH_SHORT).show()
            return true
        }
        else if(Utlis.isEmptyCheck(ed_phone.text.toString().trim()))
        {
            Toast.makeText(this,"Phone Number is requirement.",Toast.LENGTH_SHORT).show()
            return true
        }
        else if(Utlis.isEmptyCheck(ed_password.text.toString().trim()))
        {
            Toast.makeText(this,"Password is requirement.",Toast.LENGTH_SHORT).show()
            return true
        }
        else
        {
            return false
        }

    }
}