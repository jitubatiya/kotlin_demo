package com.example.kotlin_demo.ActivitySqlExample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        btn_add.setOnClickListener()
        {
            val intent = Intent(this, InsertActivity::class.java)
            startActivity(intent)
        }
        btn_display.setOnClickListener(){
            val intent = Intent(this, DisplayActivity::class.java)
            startActivity(intent)
        }
    }
}