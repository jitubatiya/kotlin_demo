package com.example.kotlin_demo.ActivitySqlExample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlin_demo.Adapter.UserAdapter
import com.example.kotlin_demo.Helper.DatabaseHandler
import com.example.kotlin_demo.Model.EmpModelClass
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.activity_display.*

class DisplayActivity : AppCompatActivity(),UserAdapter.CliclEvnt {
    val list=ArrayList<HashMap<String,String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display)
        getData()
    }

    private fun getData() {
        val databaseHandler: DatabaseHandler= DatabaseHandler(this)
        val emp: List<EmpModelClass> = databaseHandler.viewEmployee()
        var index = 0
        for(e in emp){
            var hashMap=HashMap<String,String>()
            hashMap.put("id",e.id.toString())
            hashMap.put("name",e.name)
            hashMap.put("email",e.email)
            hashMap.put("phone",e.phone)
            hashMap.put("password",e.password)

            list.add(index,hashMap)
            index++
        }
        //creating custom ArrayAdapter
        val myListAdapter = UserAdapter(list,this)
        rv_user_list.layoutManager= LinearLayoutManager(this)
        rv_user_list.adapter = myListAdapter
        myListAdapter.setOnItemClick(this)
    }

    override fun ClickMe(position: Int, view: View) {
        val hashMap=list.get(position)
        val intent= Intent(this,EditActivity::class.java)
        intent.putExtra("userData",hashMap)
        startActivity(intent)
    }
}