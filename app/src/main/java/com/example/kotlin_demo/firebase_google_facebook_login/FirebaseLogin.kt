package com.example.kotlin_demo.firebase_google_facebook_login

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.example.kotlin_demo.R
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_firebase_login.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class FirebaseLogin : AppCompatActivity() {
    companion object{
        var RC_SIGN_IN=123
        var FaceBook=123
    }
    var bool:Boolean=false
    private  lateinit var mAuth:FirebaseAuth
    private lateinit var googleSignInClient:GoogleSignInClient
    private lateinit var callbackManager:CallbackManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase_login)

        printHashKey(this)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient=GoogleSignIn.getClient(this,gso)

        //firebase auth
        mAuth=FirebaseAuth.getInstance()

        btn_sign_in.setOnClickListener(){
            bool=false
            signIn()
        }

        btn_facebook.setOnClickListener(){
            bool=true
            faceBookSignin()
        }
    }

    private fun faceBookSignin() {
        Log.d("FacebookLogin", "hello")
        callbackManager = CallbackManager.Factory.create()

        btn_facebook.setReadPermissions("email", "public_profile")
        btn_facebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d("FacebookLogin", "facebook:onSuccess:$loginResult")
                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                Log.d("FacebookLogin", "facebook:onCancel")
                // ...
            }

            override fun onError(error: FacebookException) {
                Log.d("FacebookLogin", "facebook:onError", error)
                // ...
            }
        })

    }
    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d("FacebookLogin", "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful)
                {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("FacebookLogin", "signInWithCredential:success")
                    val user = mAuth.currentUser
                    //updateUI(user)
                    val intent=Intent(this,GoogleDataDisplay::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("FacebookLogin", "signInWithCredential:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                   // updateUI(null)
                }

                // ...
            }
    }
    private fun signIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val exception=task.exception
            if(task.isSuccessful)
            {
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)!!
                    Log.d("FirebaseLogin", "firebaseAuthWithGoogle:" + account.id+""+account.email)
                    firebaseAuthWithGoogle(account.idToken!!)
                } catch (e: ApiException) {
                    // Google Sign In failed, update UI appropriately
                    Log.w("FirebaseLogin", "Google sign in failed", e)
                    // ...
                }
            }
            else
            {
                Log.d("FirebaseLogin", exception.toString() )
            }
        }
        if(bool)
        {
            callbackManager.onActivityResult(requestCode, resultCode, data)

        }
    }
    public override fun onStart() {
        super.onStart()
        googleSignInClient.signOut()

    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    // Sign in success, update UI with the signed-in user's information
                    Log.d("FirebaseLogin", "signInWithCredential:success")
                    val user = mAuth.currentUser
                    val intent=Intent(this,GoogleDataDisplay::class.java)
                    startActivity(intent)
                    finish()
                    //updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("FirebaseLogin", "signInWithCredential:failure", task.exception)
                    // ...
                    //Snackbar.make(view, "Authentication Failed.", Snackbar.LENGTH_SHORT).show()
                    //updateUI(null)
                }

                // ...
            }
    }
    fun printHashKey(pContext: Context) {
        try {
            val info: PackageInfo = pContext.getPackageManager()
                .getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.d("HashKey", "printHashKey() Hash Key:"+ hashKey)
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("HashKey", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("HashKey", "printHashKey()", e)
        }
    }
}