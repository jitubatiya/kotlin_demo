package com.example.kotlin_demo.firebase_google_facebook_login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.kotlin_demo.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_google_data_display.*

class GoogleDataDisplay : AppCompatActivity() {
    private  lateinit var mAuth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_data_display)

        mAuth=FirebaseAuth.getInstance()
        val currentuser=mAuth.currentUser
        txtName.setText("Name:"+currentuser?.displayName)
        txtEmail.setText("email:"+currentuser?.email)
        Glide.with(this).load(currentuser?.photoUrl).into(imgProfilePic)

        btn_sign_out.setOnClickListener(){
            mAuth.signOut()
            val intent=Intent(this,FirebaseLogin::class.java)
            startActivity(intent)
            finish()
        }
    }
}