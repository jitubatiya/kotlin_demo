package com.example.kotlin_demo.api_demo_api.ApiHelper

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class ExecuteWebServerUrl {
    var setDataRes: SetDataResponse? = null

    lateinit var parameters: HashMap<String, String>

    var generalFunc: GeneralFunctions? = null

    var responseString = ""

    var directUrl_value = false
    var directUrl:String = ""

    var isLoaderShown = false
    var mContext: Context? = null

    lateinit var myPDialog: MyProgressDialog

    var isGenerateDeviceToken = false
    var key_DeviceToken_param: String? = null
    var isTaskKilled = false

    constructor(parameters: HashMap<String, String>) {
        val c = Calendar.getInstance()
        val date = c.time
        @SuppressLint("SimpleDateFormat") val df =
            SimpleDateFormat("mm")
        df.timeZone = TimeZone.getTimeZone("GMT+2")
        val strDate = df.format(date)
            //parameters["token"] = MyCrypt.encrypt_data(strDate)
        //        Utils.printLog("secret_token",parameters.toString());
        this.parameters = parameters
    }

    constructor(
        directUrl: String,
        directUrl_value: Boolean,
        parameters: HashMap<String, String>
    ) {
        this.directUrl = directUrl
        this.directUrl_value = directUrl_value
        this.parameters = parameters
    }

    fun setLoaderConfig(
        mContext: Context?,
        isLoaderShown: Boolean,
        generalFunc: GeneralFunctions?
    ) {
        this.isLoaderShown = isLoaderShown
        this.generalFunc = generalFunc
        this.mContext = mContext
    }

    fun setIsDeviceTokenGenerate(
        isGenerateDeviceToken: Boolean,
        key_DeviceToken_param: String?
    ) {
        this.isGenerateDeviceToken = isGenerateDeviceToken
        this.key_DeviceToken_param = key_DeviceToken_param
    }

    fun execute() {
        myPDialog = MyProgressDialog(mContext, false, "Loading")
        if (isLoaderShown == true) {

            myPDialog.show()
        }
        if (directUrl_value == false) {
            performPostCall()
        } else {
            responseString = ExecuteResponse.getResponse(directUrl).toString()
        }
    }

    fun performPostCall() {
        var call: Call<Object>
        if(parameters.get("TAG").equals("login"))
           call= RestClient.getClient().getResponse(parameters)
        else
            call= RestClient.getClient().getResponse(directUrl)
        call.enqueue(object : Callback<Object> {

            override fun onResponse(
                call: Call<Object>,
                response: Response<Object>
            ) {
                if (response.isSuccessful) {
                    // request successful (status code 200, 201)

//                    Utils.printLog("Data", "response = " + new Gson().toJson(response.body()));
                    responseString = RestClient.getGSONBuilder().toJson(response.body())
                    fireResponse()
                } else {
                    responseString = ""
                    fireResponse()
                }
            }

            override fun onFailure(
                call: Call<Object>,
                t: Throwable
            ) {
                Log.d("DataError",t.message.toString())

                responseString = ""
                fireResponse()
            }
        })
    }

    fun cancel() {
        isTaskKilled = true
    }

    fun fireResponse() {
        if (myPDialog != null) {
            myPDialog.close()
        }
        if (setDataRes != null && isTaskKilled == false) {
            setDataRes!!.setResponse(responseString)
        }
    }


    interface SetDataResponse {
        fun setResponse(responseString: String)
    }

    fun setDataResponseListener(setDataRes: SetDataResponse) {
        this.setDataRes = setDataRes
    }
}