package com.example.kotlin_demo.api_demo_api.ApiHelper

import android.content.Context
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class GeneralFunctions {
        lateinit var mContext:Context

        fun GeneralFunctions(mContext: Context) {
            this.mContext = mContext
        }

        fun getJsonObject(key: String?, responseString: String?): JSONObject? {
            return try {
                val obj_temp = JSONObject(responseString)
                obj_temp.getJSONObject(key)
            } catch (e: JSONException) {
                e.printStackTrace()
                null
            }
        }
        fun getJsonValue(key: String,obj: JSONObject):String
        {
            try {
                val value_str = obj.getString(key)
                if (value_str != null && value_str != "null" && value_str != "") {
                    return value_str
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
            return ""
        }
    fun getJsonObject(arr: JSONArray, position: Int): JSONObject {
         try {
           return arr.getJSONObject(position)
        } catch (e: JSONException) {
            e.printStackTrace()
            return JSONObject()
        }
    }
    fun getJsonArray(key: String, obj: JSONObject): JSONArray {
         try {
            return   obj.getJSONArray(key)
        } catch (e: JSONException) {
            e.printStackTrace()
             return JSONArray()

         }

    }


}