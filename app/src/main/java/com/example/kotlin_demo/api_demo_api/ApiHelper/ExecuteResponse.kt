package com.example.kotlin_demo.api_demo_api.ApiHelper

import java.io.*
import java.net.HttpURLConnection
import java.net.URL

 class ExecuteResponse
{
    companion object {


         fun getResponse(url_str: String?): String? {
            var responseString: String? = ""
            var urlConnection: HttpURLConnection? = null
            try {
                //Log.d("url_str", "url_str::" + url_str);
                val url = URL(url_str)
                urlConnection = url.openConnection() as HttpURLConnection
                val `in`: InputStream =
                    BufferedInputStream(urlConnection.inputStream)
                responseString =
                    readStream(
                        `in`
                    )
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
                responseString = ""
            } finally {
                urlConnection?.disconnect()
            }
            return responseString
        }

        @Throws(IOException::class)
        fun readStream(`is`: InputStream): String? {
            val sb = StringBuilder()
            val r =
                BufferedReader(InputStreamReader(`is`), 1000)
            var line = r.readLine()
            while (line != null) {
                sb.append(line)
                line = r.readLine()
            }
            `is`.close()
            return sb.toString()
        }

        @Throws(IOException::class)
        private fun convertToByteArray(inputStream: InputStream): ByteArray? {
            val bos = ByteArrayOutputStream()
            var next = inputStream.read()
            while (next > -1) {
                bos.write(next)
                next = inputStream.read()
            }
            bos.flush()
            return bos.toByteArray()
        }
    }
}