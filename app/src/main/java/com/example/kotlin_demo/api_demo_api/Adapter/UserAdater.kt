package com.example.kotlin_demo.api_demo_api.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.activity_google_data_display.*
import kotlinx.android.synthetic.main.user_list_item1.view.*

class UserAdapter(var items: ArrayList<HashMap<String, String>>, val context: Context) : RecyclerView.Adapter<ViewHolder>()
{

    // Gets the number of animals in the list
    lateinit var obj:CliclEvnt
    override fun getItemCount(): Int {
        return items.size
    }
    interface CliclEvnt{
        fun ClickMe(position: Int,view: View);
    }
    fun setOnItemClick(obj:CliclEvnt)
    {
        this.obj=obj
    }
    // Inflates the item views
    fun addData(listItems: ArrayList<HashMap<String, String>>) {
        var size = this.items.size
        this.items.addAll(listItems)
        var sizeNew = this.items.size
        notifyItemRangeChanged(size, sizeNew)
    }
    fun updateData(listItems: ArrayList<HashMap<String, String>>)
    {
        this.items=listItems

        notifyDataSetChanged()
    }
    // Binds each animal in the ArrayList to a view

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.user_list_item1, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hashmap=items.get(position)

        holder.main_view.setOnClickListener(){
                obj.ClickMe(position,holder.main_view)
        }
        holder.id.text="Id:"+hashmap.get("id")
        holder.name.text="Name:"+hashmap.get("first_name")+" "+hashmap.get("last_name")

        holder.email.text="Email:"+hashmap.get("email")
        Glide.with(context).load(hashmap.get("avatar")).into(holder.img_prof)

    }

    fun getList(): ArrayList<HashMap<String, String>> {
        return items
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val id = view.txtid
    val name = view.txtname

    val main_view=view.linearLayout

    val email=view.txtemail
    val img_prof=view.img_pic
}