package com.example.kotlin_demo.api_demo_api.ApiHelper

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

open class RestClient {
    companion object {
        private lateinit var apiInterface: ApiInterface
        var tag="login"
        private const val url="https://reqres.in/api/login"
        private const val url1="https://reqres.in/api/users"
        private const val baseUrl="https://reqres.in/api/"


         fun getClient(): ApiInterface
         {

            val okClient = OkHttpClient().newBuilder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(50, TimeUnit.SECONDS)
                .writeTimeout(50, TimeUnit.SECONDS)
                .build()
            val client: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                apiInterface = client.create(ApiInterface::class.java)
            return apiInterface
        }
        fun getGSONBuilder(): Gson {
            return GsonBuilder().registerTypeAdapter(
                Double::class.java,
                JsonSerializer<Double> { src, typeOfSrc, context ->
                    if (src == src.toLong()
                            .toDouble()
                    ) JsonPrimitive("" + src.toLong()) else JsonPrimitive("" + src)
                }).create()
        }

        interface ApiInterface
        {

            @FormUrlEncoded
            @POST(url)
            fun getResponse(@FieldMap params: Map<String, String>): Call<Object>

            @FormUrlEncoded
            @POST(url1)
            fun getResponse1(@FieldMap params: Map<String, String>): Call<Object>
            @GET
            fun getResponse(@Url url: String): Call<Object>

            @Multipart
            @POST("")
            fun uploadData(
                @Part file: MultipartBody.Part?,
                @PartMap params: Map<String?, RequestBody?>?
            ): Call<Any?>?
        }
    }
}