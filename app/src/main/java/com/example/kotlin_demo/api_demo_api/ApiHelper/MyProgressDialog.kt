package com.example.kotlin_demo.api_demo_api.ApiHelper

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.kotlin_demo.R

class MyProgressDialog {
    var mContext: Context? = null
    var cancelable = false
   lateinit var my_progress_dialog: Dialog

    constructor(
        mContext: Context?,
        cancelable: Boolean,
        message_str: String?
    ) {
        this.mContext = mContext
        this.cancelable = cancelable
        build()
        setMessage(message_str)
    }

    fun build() {
        my_progress_dialog = Dialog(mContext!!, R.style.theme_my_progress_dialog)
        my_progress_dialog.setContentView(R.layout.my_progress_dilalog_design)
        val window = my_progress_dialog!!.window
        window!!.setGravity(Gravity.CENTER)
        window.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        my_progress_dialog!!.window!!.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        my_progress_dialog!!.setCanceledOnTouchOutside(false)
        my_progress_dialog!!.setCancelable(cancelable)
    }

    fun setMessage(msg_str: String?) {
        val msgTxt =
            my_progress_dialog!!.findViewById<View>(R.id.msgTxt) as TextView
        msgTxt.text = msg_str
    }

    fun show() {
        my_progress_dialog!!.show()
    }

    fun close() {
        try {
            my_progress_dialog!!.dismiss()
        } catch (e: Exception) {
        }
    }
}