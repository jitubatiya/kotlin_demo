package com.example.kotlin_demo.api_demo_api.Activity

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.kotlin_demo.R
import com.example.kotlin_demo.api_demo_api.Adapter.UserAdapter
import com.example.kotlin_demo.api_demo_api.ApiHelper.ExecuteWebServerUrl
import com.example.kotlin_demo.api_demo_api.ApiHelper.GeneralFunctions
import com.example.kotlin_demo.api_demo_api.Pagination_helper.PaginationScrollListener
import com.facebook.internal.Utility.arrayList
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_google_data_display.*
import kotlinx.android.synthetic.main.activity_home.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class HomeActivity : AppCompatActivity(), UserAdapter.CliclEvnt {
    lateinit var list:ArrayList<HashMap<String,String>>
    lateinit var generalFunc:GeneralFunctions
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var isEnd:Boolean=false
    var pageIndex:Int=1
    var pos:Int=0

    lateinit  var myListAdapter:UserAdapter
    lateinit var layoutManager:LinearLayoutManager
    var sort_arr= arrayOf("Default","ascending","descending")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        list=ArrayList<HashMap<String,String>>()
        myListAdapter= UserAdapter(list,applicationContext)
        generalFunc= GeneralFunctions()
        layoutManager= LinearLayoutManager(this)


        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, sort_arr)
        sp_sort.adapter=adapter
        getData()

        rv_user_list.addOnScrollListener(object:PaginationScrollListener(layoutManager)
        {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                getMoreItems()

            }

        })

        sp_sort.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                pos=position
                sort_fun(position)
            }

        }
        edt_serach.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                filter(s.toString().trim())

            }

        });

    }
     fun filter(value:String)
    {
        var arrayList=ArrayList<HashMap<String,String>>()
        if(myListAdapter.getList().size==0)
            arrayList= list
        else
            arrayList=myListAdapter.getList()


        if(value.isNotEmpty()) {
            var list_filter = ArrayList<HashMap<String, String>>()
            for (i in 0..arrayList.size - 1) {
                var obj = arrayList.get(i) as HashMap<String, String>
                if (obj.get("first_name").toString().toUpperCase().contains(value.toString().toUpperCase())) {
                    list_filter.add(obj)
                }
            }
            myListAdapter.updateData(list_filter)
        }
        else
        {
            myListAdapter.updateData(list)
            sort_fun(pos)
        }
    }

    private fun sort_fun(position: Int)
    {


            var arrayList = myListAdapter.getList()
            val distanceComparator: Comparator<HashMap<String, String>> =
                object : Comparator<HashMap<String, String>> {


                    override fun compare(
                        o1: HashMap<String, String>,
                        o2: HashMap<String, String>
                    ): Int {
                        if (position == 2) {
                            val distance1 = o1.get("first_name")
                            val distance2 = o2.get("first_name")
                            return distance2.toString().compareTo(distance1.toString())
                        } else if (position == 1) {
                            val distance1 = o1.get("first_name")
                            val distance2 = o2.get("first_name")
                            return distance1.toString().compareTo(distance2.toString())
                        } else {
                            val distance1 = o1.get("id").toString().toFloat()
                            val distance2 = o2.get("id").toString().toFloat()
                            return distance1.compareTo(distance2)
                        }
                    }
                }
            Collections.sort(arrayList, distanceComparator)
            myListAdapter.updateData(arrayList)


    }
    private fun getMoreItems() {
        if(!isEnd) {
            pageIndex = pageIndex + 1
            isLoading = false
            getData()
        }


    }

    private fun getData() {
        val parameters = HashMap<String, String>()
        parameters.put("TAG","users")


        val exeWebServer= ExecuteWebServerUrl("https://reqres.in/api/users?page="+pageIndex,false,parameters)
        exeWebServer.setLoaderConfig(this,true,generalFunc)
        exeWebServer.setDataResponseListener(object : ExecuteWebServerUrl.SetDataResponse
        {
            override fun setResponse(responseString: String) {

                if (responseString != null && responseString != "")
                {
                    Log.d("data123",responseString)
                    val obj=JSONObject(responseString)
                    var array: JSONArray =generalFunc.getJsonArray("data",obj)
                    if(array.length()>0)
                    {
                        for(i in 0..array.length()-1)
                        {

                            var map=HashMap<String,String>()
                            var obj=generalFunc.getJsonObject(array,i)
                            map.put("email",generalFunc.getJsonValue("email",obj))
                            map.put("id",generalFunc.getJsonValue("id",obj))
                            map.put("first_name",generalFunc.getJsonValue("first_name",obj))
                            map.put("avatar",generalFunc.getJsonValue("avatar",obj))
                            map.put("last_name",generalFunc.getJsonValue("last_name",obj))
                            list.add(map)
                        }
                        if(pageIndex==1) {
                            myListAdapter = UserAdapter(list, applicationContext)
                            rv_user_list.layoutManager = layoutManager
                            rv_user_list.adapter = myListAdapter
                            myListAdapter.setOnItemClick(this@HomeActivity)
                        }
                        else
                        {

                            myListAdapter.updateData(list)
                        }
                    }
                    else
                    {
                        isEnd=true
                    }

                }
            }
        })
        exeWebServer.execute()
    }

    override fun ClickMe(position: Int, view: View)
    {
        val obj=list.get(position)
        showDialog(obj)
    }
    private fun showDialog(obj:HashMap<String,String>) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.custom_layout)
        val txt_name = dialog.findViewById(R.id.txt_name) as TextView
        val txtEmail = dialog.findViewById(R.id.txt_mail) as TextView
        val prof_image=dialog.findViewById(R.id.profile_image) as CircleImageView
        txt_name.text=obj.get("first_name")+" "+obj.get("last_name")
        Glide.with(this).load(obj.get("avatar")).into(prof_image)
        txtEmail.text="mail:"+obj.get("email")
        //body.text = title
        //val yesBtn = dialog.findViewById(R.id.yesBtn) as Button
        //val noBtn = dialog.findViewById(R.id.noBtn) as TextView
        //yesBtn.setOnClickListener {
          //  dialog.dismiss()
        //}
        //noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

}