package com.example.kotlin_demo.api_demo_api.Activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlin_demo.R
import com.example.kotlin_demo.api_demo_api.ApiHelper.ExecuteWebServerUrl
import com.example.kotlin_demo.api_demo_api.ApiHelper.GeneralFunctions
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.util.*

class MainActivity : AppCompatActivity() {
    var arr=Array(5){0}
    lateinit var  generalFunc:GeneralFunctions
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        generalFunc= GeneralFunctions()
        r1.setOnClickListener() {
            checkValidation()
        }
        txtsignin.setOnClickListener(){
            checkValidation()
        }
        txtsignup.setOnClickListener(){

        }

    }
    fun checkValidation()
    {

        var intent= Intent(this@MainActivity,HomeActivity::class.java)
        startActivity(intent)
        finish()

        if(!isValidEmail(edit_email.text.toString()))
           Toast.makeText(this@MainActivity,"Email address invalid.",Toast.LENGTH_SHORT).show()
        else if(edit_password.text.length<=0)
            Toast.makeText(this@MainActivity,"Password is empty..",Toast.LENGTH_SHORT).show()
        else
            checkLogin()
    }

    private fun checkLogin() {
        val parameters = HashMap<String, String>()
        parameters.put("TAG","login")
        parameters.put("email",edit_email.text.toString())
        parameters.put("password",edit_password.text.toString())

        val exeWebServer=ExecuteWebServerUrl(parameters)
        exeWebServer.setLoaderConfig(this,true,generalFunc)
        exeWebServer.setDataResponseListener(object : ExecuteWebServerUrl.SetDataResponse
        {
            override fun setResponse(responseString: String) {
                if (responseString != null && responseString != "")
                {
                    Log.d("data123",responseString)
                    val obj=JSONObject(responseString)
                     var token= generalFunc.getJsonValue("token",obj)
                    if(!token.isEmpty())
                    {
                        Toast.makeText(this@MainActivity,"Login Sucessfully",Toast.LENGTH_SHORT).show()
                        var intent= Intent(this@MainActivity,HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    else
                    {
                        Toast.makeText(this@MainActivity,"Invalid input",Toast.LENGTH_SHORT).show()
                    }
                }
                else
                {
                    Toast.makeText(this@MainActivity,"  Note: Only defined users succeed registration",Toast.LENGTH_SHORT).show()
                }
            }
        })
        exeWebServer.execute()
    }

    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }



}