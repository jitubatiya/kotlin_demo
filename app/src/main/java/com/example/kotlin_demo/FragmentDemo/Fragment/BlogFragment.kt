package com.example.kotlin_demo.FragmentDemo.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.kotlin_demo.R
import kotlinx.android.synthetic.main.fragment_common.*

class BlogFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_common, container, false)

        override fun onActivityCreated(savedInstanceState: Bundle?)
        {
        super.onActivityCreated(savedInstanceState)
            tvCommon.text = "Blog Fragment"
            commonLayout.setBackgroundColor(resources.getColor(android.R.color.darker_gray))

    }
}
